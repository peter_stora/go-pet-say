package models

import "fmt"

type Cat struct {
	Pet
}

func (c Cat) Sound() string {
	return fmt.Sprintf("%s says %s", c.Name, "mjau")
}
