package models

type IPet interface {
	Sound() string
}

type Pet struct {
	Name       string
	AnimalType string
}

func (p Pet) Sound() string {
	// switch p.AnimalType {
	// case "cat":
	// 	return fmt.Sprintf("%s says %s", p.Name, "mjau")
	// case "dog":
	// 	return fmt.Sprintf("%s says %s", p.Name, "woof")
	// default:
	// 	return "hello"
	// }
	return "hello"
}
