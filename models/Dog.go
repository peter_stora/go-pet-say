package models

import (
	"fmt"
)

type Dog struct {
	Pet
}

func (d Dog) Sound() string {
	return fmt.Sprintf("%s says %s", d.Name, "woof")
}
