package main

import (
	"fmt"
	"go-pet-say/dal"
)

func main() {
	pet, err := dal.GetPet("Blixten")
	if err == nil {
		fmt.Println(pet.Sound())
	}
}
