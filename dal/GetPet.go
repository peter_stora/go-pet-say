package dal

import (
	"go-pet-say/models"

	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func GetPet(name string) (models.IPet, error) {
	sess := session.Must(session.NewSession())
	db := dynamodb.New(sess)

	/*keyMap, err := dynamodbattribute.MarshalMap(models.Pet{Name: name})
	if err != nil {
		fmt.Printf("error, failed to marshal: %s", err.Error())
		return nil, err
	}*/

	item, err := db.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("Pets"),
		//Key:       keyMap,
		Key: map[string]*dynamodb.AttributeValue{
			"Name": &dynamodb.AttributeValue{
				S: aws.String(name),
			},
		},
	})

	if err != nil {
		fmt.Printf("error: %s", err.Error())
		return nil, err
	} else {
		fmt.Printf("success: %s", item.Item)

		var pet models.Pet
		err := dynamodbattribute.UnmarshalMap(item.Item, &pet)

		if err != nil {
			fmt.Printf("error: %s", err.Error())
			return nil, err
		}

		ourPet, err := createPet(pet.AnimalType, pet.Name)
		if err != nil {
			fmt.Printf("error: %s", err.Error())
			return nil, err
		}

		return ourPet, nil
	}
}

func createPet(animalType string, name string) (models.IPet, error) {
	switch animalType {
	case "dog":
		return models.Dog{models.Pet{Name: name, AnimalType: animalType}}, nil
	case "cat":
		return models.Cat{models.Pet{Name: name, AnimalType: animalType}}, nil
	default:
		return nil, nil
	}
}
